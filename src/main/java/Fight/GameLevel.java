package Fight;

import DroidModels.*;
import Input.UserInput;
import Random.Random;
import Scene.Scene;

/**
 * Represent game level
 */
public class GameLevel {
    private static int lvl;
    private static String[] namesOfLevel = {"51 Pegassa b", "System u Andromeda",
            "OGLE-235/MOA-53", "KOI-961 d ", "Kepler-20 e"};

    /**
     * Begin game level
     * And starts fight
     * If user win previous level, he go to next level
     *
     * @param heroDroid droid that was chosen by user
     */
    public static void startLevel(Droid heroDroid) {
        lvl++;

        if (lvl > 5) {
            System.out.println("Congratulations! You destroy army of Galactic Empire.");
            return;
        }
        if (lvl == 5) {
            Scene.showBossFight();
        }
        chooseCard(heroDroid);
        showCurrentLevel();

        heroDroid.setLevel(lvl);

        FightCreator fightCreator = new FightCreator(heroDroid, randomEnemy());
        fightCreator.fight();

    }

    /**
     * Generate random enemy droid
     *
     * @return random enemy droid
     */
    private static Droid randomEnemy() {

        int randomEnemy = Random.generateRandom();

        switch (randomEnemy) {
            case 1: {
                return new BalancedDroid("Balanced Droid", lvl - 1);
            }
            case 2: {
                return new BattleDroid("Battle Droid", lvl - 1);
            }
            case 3: {
                return new ProtectedDroid("Protected Droid", lvl - 1);
            }
            default: {
                return null;
            }
        }
    }

    /**
     * Print current level and location
     */
    private static void showCurrentLevel() {
        System.out.println("                            " +
                "Current level : " + lvl);
        System.out.println("                            " +
                "Location : " + namesOfLevel[lvl - 1]);
        System.out.println("_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ " +
                "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _");
    }

    /**
     * Print available upgrade cards
     * User must choose one of them
     *
     * @param heroDroid that was chosen by user
     */
    private static void chooseCard(Droid heroDroid) {
        CardsBase cardsBase = new CardsBase();
        cardsBase.generateCards();
        System.out.println("Choose the upgrade card\n");
        System.out.println("Card #1 --- Press 1");
        System.out.println("Card #2 --- Press 2");
        System.out.println("Card #3 --- Press 3");

        int choiceInput = UserInput.inputFromOneToThree();
        heroDroid.upGrade(cardsBase.cards[choiceInput - 1]);
        System.out.println("Now your stats is");
        Scene.showDroidInfo(heroDroid);

    }
}
