package Fight;

/**
 * Represent cards that upgrade Droid
 */
public class UpgradeCard {
    private int upgradeHealth;
    private int upgradeProtection;
    private int upgradeAttack;

    UpgradeCard(int uH, int uP, int uA) {
        upgradeAttack = uA;
        upgradeProtection = uP;
        upgradeHealth = uH;
    }

    public int getUpgradeHealth() {
        return upgradeHealth;
    }

    public int getUpgradeProtection() {
        return upgradeProtection;
    }

    public int getUpgradeAttack() {
        return upgradeAttack;
    }

    @Override
    public String toString() {
        return "\n_______________________________\n" +
                "\n Health Upgrade = " + upgradeHealth +
                "\n Protection Upgrade = " + upgradeProtection +
                "\n Attack Upgrade = " + upgradeAttack;
    }
}
