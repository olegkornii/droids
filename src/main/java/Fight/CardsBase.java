package Fight;

/**
 * Represent Upgrade card database
 */
class CardsBase {
    UpgradeCard[] cards = new UpgradeCard[3];

    /**
     * generate 3 random upgrade cards
     */
    void generateCards() {
        for (int i = 0; i < cards.length; i++) {
            cards[i] = new UpgradeCard(gCS(), gCS(), gCS());
        }
        showCards();
    }

    /**
     * @return random int from -2 to 3
     */
    private int gCS() {
        return (int) (Math.random() * 5 - 2);
    }

    /**
     * print all available cards from base
     */
    private void showCards() {
        for (int i = 0; i < cards.length; i++) {
            System.out.println("Card # " + (i + 1) + " " + cards[i]);
        }
    }
}
