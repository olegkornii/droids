package Fight;

/**
 * BossPower class generate ups for boss droid
 * regeneration - points added to health
 * avoidance - percentage of hero`s attack decrease
 * criticalHint - percentage of enemy`s attack increase
 */

public class BossPower {
    private int regeneration;
    private int avoidance;
    private int criticalHit;

    BossPower() {
        regeneration = getRegenerationPoints();
        avoidance = getPercentage();
        criticalHit = getPercentage();
    }

    int getRegeneration() {
        System.out.println("Enemy regen " + regeneration);
        return regeneration;
    }


    int getAvoidance() {
        System.out.println("Your attack was lowered by " + avoidance + "%");
        return avoidance;
    }

    int getCriticalHit() {
        System.out.println("Enemy attack increase by " + criticalHit + "%");
        return criticalHit;
    }

    private int getRegenerationPoints() {
        return (int) (Math.random() * 5);
    }

    private int getPercentage() {
        return (int) (Math.random() * 7);
    }
}
