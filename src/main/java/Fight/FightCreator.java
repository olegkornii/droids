package Fight;

import Activity.Action;
import Activity.BodyPart;
import DroidModels.Droid;
import Input.UserInput;
import Random.Random;
import Scene.Scene;

/**
 * Represent fight between two droids
 */
class FightCreator {

    private Droid heroDroid;
    private Droid enemyDroid;
    private Action heroAction;
    private Action enemyAction;

    /**
     * Initialize two droids, that will fight
     *
     * @param heroDroid  Droid that was chosen by user
     * @param enemyDroid Enemy droid
     */
    FightCreator(Droid heroDroid, Droid enemyDroid) {
        this.heroDroid = heroDroid;
        this.enemyDroid = enemyDroid;
    }

    /**
     * Represent fight between two droids
     * If user win fight, he goes to next level
     * If not game ends.
     */
    void fight() {
        do {
            heroAction = new Action(selectPart("that you wanna attack"),
                    selectPart("that you wanna defence"));
            enemyAction = new Action(enemyDroidSelectPart(), enemyDroidSelectPart());


            if (heroDroid.getLevel() == 5) {
                BossPower bp = new BossPower();

                heroDroid.setAttack(heroDroid.getAttack()
                        - heroDroid.getAttack() * bp.getAvoidance() / 100);

                enemyDroid.setAttack(enemyDroid.getAttack()
                        + enemyDroid.getAttack() * bp.getCriticalHit() / 100);

                enemyDroid.setHealth(enemyDroid.getHealth() + bp.getRegeneration());
            }

            successOfTheAttack();

            currentStats();

            if (enemyDroid.getHealth() <= 0
                    && heroDroid.getHealth() > 0) {

                System.out.println("You win");
                GameLevel.startLevel(heroDroid);
            } else if (heroDroid.getHealth() <= 0) {

                System.out.println("You lose");
                break;
            }
        } while (heroDroid.getHealth() > 0
                && enemyDroid.getHealth() > 0);
    }

    /**
     * @param msg message that user see
     * @return part of body that user want to attack of defence
     */
    private BodyPart selectPart(String msg) {
        System.out.println("Choose part that " + msg);
        System.out.println("Head--------1*");
        System.out.println("Torso-------2*");
        System.out.println("Legs--------3*");
        int choiceInput = UserInput.inputFromOneToThree();

        return choice(choiceInput);
    }

    /**
     * @return part of body that will be defenced or attacked by enemy Droid
     */
    private BodyPart enemyDroidSelectPart() {
        int randomPart = Random.generateRandom();
        return choice(randomPart);
    }

    /**
     * Shows success of the attack
     * If attack success damage deals to health
     * If not, damage deals to protection
     */
    private void successOfTheAttack() {
        //When enemy defended
        if (heroAction.getAttackPart() == enemyAction.getDefencePart()) {

            Scene.blockSuccess(false, heroDroid.getAttack());
            enemyDroid.setProtection(enemyDroid.getProtection() - heroDroid.getAttack());

            if (enemyDroid.getProtection() < 0) {

                enemyDroid.setHealth(enemyDroid.getHealth() - Math.abs(enemyDroid.getProtection()));
                enemyDroid.setProtection(0);

            }
        }
        //When hero attacked successfully
        else if (heroAction.getAttackPart() != enemyAction.getDefencePart()) {

            Scene.attackSuccess(true, heroDroid.getAttack());
            enemyDroid.setHealth(enemyDroid.getHealth() - heroDroid.getAttack());

        }
        //When hero defended successfully
        if (heroAction.getDefencePart() == enemyAction.getAttackPart()) {

            Scene.blockSuccess(true, enemyDroid.getAttack());
            heroDroid.setProtection(heroDroid.getProtection() - enemyDroid.getAttack());

            if (heroDroid.getProtection() < 0) {

                heroDroid.setHealth(heroDroid.getHealth() - Math.abs(heroDroid.getProtection()));
                heroDroid.setProtection(0);
            }
            //When enemy attack successfully
        } else if (heroAction.getDefencePart() != enemyAction.getAttackPart()) {

            Scene.attackSuccess(false, enemyDroid.getAttack());
            heroDroid.setHealth(heroDroid.getHealth() - enemyDroid.getAttack());
        }
    }

    /**
     * @param choise choice of user or random int of enemy
     * @return part that will be defences or attacked
     */
    private BodyPart choice(int choise) {
        switch (choise) {
            case 1: {
                return BodyPart.HEAD;
            }
            case 2: {
                return BodyPart.TORSO;
            }
            case 3: {
                return BodyPart.LEGS;
            }
        }
        return null;
    }

    /**
     * Print in console urrent stats of droids
     */
    private void currentStats() {
        System.out.println("\n");
        System.out.println("            Your Droid");
        System.out.println("Current health: " + heroDroid.getHealth());
        System.out.println("Current protection:" + heroDroid.getProtection());
        System.out.println("_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _");
        System.out.println("            Enemy Droid");
        System.out.println("Current health: " + enemyDroid.getHealth());
        System.out.println("Current protection: " + enemyDroid.getProtection());
        System.out.println("_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _");
    }


}
