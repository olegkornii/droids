package Activity;

/**
 * Activity is one turn selected body parts class.
 * attackPart defines part to attack.
 * defencePart defines part to defence
 */
public class Action {

  BodyPart attackPart;
  BodyPart defencePart;

  public Action(BodyPart attackPart, BodyPart defencePart) {
    this.attackPart = attackPart;
    this.defencePart = defencePart;
  }

  public BodyPart getAttackPart() {
    return attackPart;
  }

  public void setAttackPart(final BodyPart attackPart) {
    this.attackPart = attackPart;
  }

  public BodyPart getDefencePart() {
    return defencePart;
  }

  public void setDefencePart(final BodyPart defencePart) {
    this.defencePart = defencePart;
  }
}
