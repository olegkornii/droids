package Activity;

public enum BodyPart {
  HEAD,
  TORSO,
  LEGS
}
