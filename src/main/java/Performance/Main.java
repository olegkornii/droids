package Performance;

import Scene.Scene;
import Input.UserInput;
import Fight.GameLevel;

public class Main {
    public static void main(String[] args) {
        Scene.showIntro();
        UserInput.pressAnyKeyToContinue();
        GameLevel.startLevel(UserInput.choisE());
    }
}
