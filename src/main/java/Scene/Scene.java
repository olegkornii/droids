package Scene;

import DroidModels.Droid;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.stream.Stream;

/**
 * Scene class needed to show user-friendly interface
 */
public class Scene {

    private static final int DELAY = 100;
    static final String INTRO = readLineByLine("src/resources/intro");
    static final String DROID = readLineByLine("src/resources/droid");
    static final String CHOICE = readLineByLine("src/resources/choice");
    static final String BOSS = readLineByLine("src/resources/boss");

    private static void delay(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException exp) {
        }
    }

    public static void animate(String txt, int delay) {
        String[] lines = txt.split("\\r?\\n");
        for (String s : lines) {
            System.out.println(s);
            delay(delay);
        }
    }

    /**
     * @param filePath path to file that contains scene.
     * @return String that contains formatted scene to show.
     */
    private static String readLineByLine(final String filePath) {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }
  /**
   * Outputs all Droid characteristics Such as class, health, attack, defence
   */
  public static void showDroidInfo(Droid droid) {
    String string = MessageFormat.format(DROID, droid.getHealth(),
        droid.getAttack(), droid.getProtection(), droid.getClass().getSimpleName(),
        droid.getLevel());
    System.out.println(string);
  }

    /**
     * @param isHero       if you want to show hero success output set it to true, if you want enemy -
     *                     false
     * @param attackPoints represents attack points.
     */
    public static void blockSuccess(boolean isHero, int attackPoints) {
        if (isHero) {
            System.out.println("You`ve JUST blocked enemy attack!!!!!");
            System.out.println("Enemy deal " + attackPoints + " to your protection");
        } else {
            System.out.println("Enemy block your attack(");
            System.out.println("You deal " + attackPoints + " to enemy protection");
        }
    }

    public static void attackSuccess(boolean isHero, int attackPoints) {
        if (isHero) {
            System.out.println("Your attack is successful!!!");
            System.out.println("You deal " + attackPoints + " to enemy heath");
        } else {
            System.out.println("Enemy attack is successfull!!!!!");
            System.out.println("Enemy deal " + attackPoints + " to your health");
        }
    }

    public static void showIntro() {
        animate(INTRO, DELAY);
    }

  public static void showBossFight() {
    animate(BOSS, DELAY);
  }

  public static void showChoiceOfClass() {
    animate(CHOICE, DELAY);
  }

}
