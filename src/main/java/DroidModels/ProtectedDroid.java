package DroidModels;

/**
 * ProtectedDroid extends abstract class Droid
 * This droid has powerful defense
 */

public class ProtectedDroid extends Droid {
    final static private int INITIAL_ATTACK = 6;  //for example
    final static private int INITIAL_PROTECTION = 50;

    public ProtectedDroid(String name, int level) {
        super(name, level);
        super.setAttack(INITIAL_ATTACK);
    }

    void increasePoints(int level) {
        setProtection((level - 1) * getIncrease() + INITIAL_PROTECTION);
    }
}
