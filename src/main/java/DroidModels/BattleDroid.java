package DroidModels;

/**
 * BattleDroid extends abstract class Droid
 * This droid has powerful attack
 */

public class BattleDroid extends Droid {
    final static private int INITIAL_ATTACK = 14; //for example
    final static private int INITIAL_PROTECTION = 10;

    public BattleDroid(String name, int level) {
        super(name, level);
    }

    void increasePoints(int level) {
        setAttack((level - 1) * getIncrease() + INITIAL_ATTACK);
        setProtection(INITIAL_PROTECTION);
    }
}