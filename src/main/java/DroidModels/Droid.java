package DroidModels;

/**
 * Droid is hierarchy root of all droids It declares common properties for all droids
 */

import Activity.Action;
import Fight.UpgradeCard;

public abstract class Droid {

    final static private int INITIAL_HEALTH = 50; // for example
    final static private int INCREASE = 2;

    private String name;
    private int health;
    private int attack;
    private int protection;
    private int level;
    private Action action;

    public Droid(String name, int level) {
        this.name = name;
        // this.health = INITIAL_HEALTH;
        setLevel(level);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getProtection() {
        return protection;
    }

    public void setProtection(int protection) {
        this.protection = protection;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.health = INITIAL_HEALTH;
        increasePoints(level);
        this.level = level;
    }

    public int getIncrease() {
        return INCREASE;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "Your DroidModels\n" +
                "name: '" + name + '\'' +
                ", health: " + health +
                ", attack: " + attack +
                ", protection: " + protection +
                ", level: " + level;
    }

    abstract void increasePoints(int level);

    public void upGrade(UpgradeCard uC) {
        this.health = INITIAL_HEALTH + uC.getUpgradeHealth();
        this.protection += uC.getUpgradeProtection();
        this.attack += uC.getUpgradeAttack();
    }
}
