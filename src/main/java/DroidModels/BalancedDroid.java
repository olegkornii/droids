package DroidModels;

/**
 * BattleDroid extends abstract class Droid
 * Attack and protection levels of this droid are equal
 */

public class BalancedDroid extends Droid {
    final static private int INITIAL_ATTACK = 10;  //for example
    final static private int INITIAL_PROTECTION = 20;

    public BalancedDroid(String name, int level) {
        super(name, level);
    }

    void increasePoints(int level) {
        setAttack((level - 1) * (getIncrease() / 2) + INITIAL_ATTACK);
        setProtection((level - 1) * (getIncrease() / 2) + INITIAL_PROTECTION);
    }
}
