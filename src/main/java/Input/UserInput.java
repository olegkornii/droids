package Input;

import DroidModels.ProtectedDroid;
import DroidModels.BalancedDroid;
import DroidModels.BattleDroid;
import DroidModels.Droid;
import Scene.Scene;

import java.util.Scanner;

public class UserInput {
    public static Droid choisE() {
        Scene.showChoiceOfClass();
        Scanner scanner = new Scanner(System.in);
        int l = scanner.nextInt();
        System.out.println("=== Enter your name ===");
        Scanner nameScanner = new Scanner(System.in);
        String name = nameScanner.nextLine();
        Droid selectedDroid;

        switch (l) {
            case 1: {
                selectedDroid = new BattleDroid(name, 1);
                break;
            }
            case 2: {
                selectedDroid = new ProtectedDroid(name, 1);
                break;
            }
            case 3: {
                selectedDroid = new BalancedDroid(name, 1);
                break;
            }
            default: {
                System.out.println("Try again ");
                return choisE();
            }
        }
        System.out.println(selectedDroid.toString());
        return selectedDroid;
    }

    public static int inputFromOneToThree() {
        Scanner scanner = new Scanner(System.in);
        while (!scanner.hasNextInt()) {
            scanner.next();
        }
        int choiceInput = scanner.nextInt();
        while (choiceInput != 1 && choiceInput != 2 && choiceInput != 3) {
            System.out.println("Choose correct number");
            while (!scanner.hasNextInt()) {
                scanner.next();
            }
            choiceInput = scanner.nextInt();
        }
        return choiceInput;
    }

    public static void pressAnyKeyToContinue()
    {
        System.out.println("============================== Press ENTER key to continue ===============================");
        Scanner in = new Scanner(System.in);
        in.next();
        /*try
        {
            System.in.read();
        }
        catch(Exception e)
        {}*/
    }

}
